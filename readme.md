## Template to your Jazz-Pack Documentation

# Jazz-Pack {NAME OF YOUR PACKAGE}

### Contribute with us

**If you want to be a contributor, [write to us](mailto:jazzcoreteam@gmail.com)**

Also, access the [Jazz Documentation website](https://jazz-docs.netlify.com) for more information.

### Get Started

#### Pre-requisites

In order to have this jazz-pack running, you must have:

- NodeJs v8.11.3 or greather installed
- NPM v5.6.0 or greather installed ( comes with NodeJs )

#### The Base Project

In order to have the jazz-pack running, you need to create a NodeJs project

Create a directory to your project and enter on the directory:

```bash
mkdir hello-jazz && cd jazz-hello
```

Initiate a new node project (_use --yes to use the default options_):

```bash
npm init --yes
```

Install jazz core engine using **npm**:

```bash
npm install jazz-core
```

#### Adding the Jazz-Pack in your project

Install the jazz-pack you which to use using **npm**:

```bash
npm install jazz-pack-{packname}
```

#### Specific instructions

_You can describe here specific instructions required to run your package_

#### Running the process

Use npx to run the jazz command line:

In the case below, note that the source file is not informed, so make sure you have already copied the source.xlsx file
to your root directory

```bash
npx jazz --configFile ./node_modules/jazz-pack-{packname}/dist/pack-config.js
```

Case you want to inform the source file, type:

```bash
npx jazz --configFile ./node_modules/jazz-pack-{packname}/dist/pack-config.js --param1 paramvalue1 --param2 paramvalue2
```

#### Input Parameters:

There are 3 levels of input parameters to each jazz-pack:

- core level: valid to ALL jazz-packs
- pluign level: valid to the plugins that are used by the jazz-pack
- pack level: valid to the specific jazz-pack

By default, the jazz command line accepts the following arguments, **for any jazz-pack**:

| Name       |                                    Description                                    |
| :--------- | :-------------------------------------------------------------------------------: |
| configFile |                 the jazz-pack Configuration File path. [required]                 |
| debug      |                                   true or false                                   |
| decryptKey | the key to decrypt the jazz-pack file (_in case the jazz-pack file is encrypted_) |

<br />
<br />

These are the input parameters available for the jazz-plugins used in this jazz-pack:

| Name   |                 Description                 |
| :----- | :-----------------------------------------: |
| param1 | Description parameter 1. **Default: value** |
| param2 | Description parameter 2. **Default: value** |

<br />
<br />

These are the input parameters available for this specific jazz-pack:

| Name            |                      Description                       |
| :-------------- | :----------------------------------------------------: |
| specific_param1 | Description of the specific param 1 **Default: value** |
| specific_param2 | Description of the specific param 2 **Default: value** |
| <br />          |

> **Note**: any required note

## End Template

### The rest of documentation is specific to teach you about the jazz-packs. It is not required to who will use your package. You can delete the documentation until the end.

# Jazz Packs Documentation

### Purpose

The purpose of this documentation is to explain the concept of the **jazz-packs** and to guide the Jazz Core Team of developers on how to create and configure these components.

> **Note**: in the near future, the idea is that the jazz-packs will be generated automatically by a graphical user-friendly interface, that will allow users with minimal technical knowledge to be able to create the packs. At this moment, since this interface is still not available, the purpose of this documentation is to allow the developers from jazz core team to move on with creation and configuration of jazz-packs that will cover the most popular demands _(like Febraban layout in Brazil for example)_.

## What is a jazz-pack file?

In order to explain the concept of the jazz-packs, it is important to take a step back and explain the whole ecosystem of Jazz. The Jazz Ecosystem is composed basically by 3 types of components (that in the end are nothing more than node modules):
To make it easier to understand, think of it as a car:

- **jazz-core**: this is the main engine. Basically is a task runner that receives the instructions through the jazz-pack and processes the tasks in a pipeline. It also has a single native plugin embedded inside it, that is the Transformer plugin (by the way, this is the reason for the name of the product! If you like Transformers you will understand :-). The Transformer Plugin will be explained moving forward.

- **jazz-plugins**: These are the parts of the car, such as wheels, the multimedia system, the onboard computer, the braking system, and so on. These are the jazz-plugins. The way that Jazz application was designed, it is possible to create plugins or extensions for the most diverse purposes. For example, we can have a plugin that reads data from an excel spreadsheet and saves into a plain javascript object, another plugin that gets a javascript array, and saves the rows into a text file.
  Each plugin can have one or many tasks. Each task can be configured to receive an unlimited number of input parameters.

- **jazz-packs**: This is the car driver. Or you can think of it as a list of instructions necessary for the processing of the pipeline. Basically, there are 2 main information that need to exist on a jazz-pack file: the pipeline and each plugin configuration.

Technically speaking , the jazz-pack is nothing more than a javascript file, that will export a javascript object, with a bunch of properties.

Here we have an example of a jazz-pack file. Each of the properties will be explained in a separated section on this documentation.

```javascript
module.exports = {
  pipeline: [
    `${excelExtractorPluginPath}:jazz-plugin-excelextractor:extract`,
    `${excelExtractorPluginPath}:jazz-plugin-excelextractor:validate`,
    `${transformerPluginPath}:Transformer:transform`,
    `${textLoaderPluginPath}:jazz-plugin-textloader:load`,
  ],
  plugins: [
    {
      name: 'jazz-plugin-excelextractor',
      sheet: 'source',
    },
    {
      name: 'Transformer',
      groupIdColumn: 'groupid',
      tasks: {
        transform: {
          rawDataFrom: 'extract',
        },
      },
    },
    {
      name: 'jazz-plugin-textloader',
      tasks: {
        load: {
          rawDataFrom: 'transform',
        },
      },
    },
  ],

  userInputParameters: {
    customer_country_code: {
      alias: 'ccc',
      describe: 'Customer Country Code',
      demandOption: false,
      default: '320',
    },
    ...
  },

```

<a name="pipeline"></a>

### .pipeline [ ]

This is an array that contains all the steps to be executed. It can have as many steps as you want:

```javascript
pipeline: [
  `${excelExtractorPluginPath}:jazz-plugin-excelextractor:extract`,
  `${excelExtractorPluginPath}:jazz-plugin-excelextractor:validate`,
  `${transformerPluginPath}:Transformer:transform`,
  `${textLoaderPluginPath}:jazz-plugin-textloader:load`
];
```

In the pipeline above, there are 4 steps:

1. extract data from an excel file and save into a javascript object
2. validate the extracted data
3. transform the data
4. load the transformed data into a text file

> Important: the steps will executed by Jazz in the sequence they are informed in the array.

Note that the steps will always be tasks. These tasks are ES6 Classes that inherit a Task Class in Jazz core lib. They can be in 3 different places:

- External plugins (that is the case of the steps 1,2 and 4)
- Native plugins (in fact, we just have one that is the Transformer) (that is the case of the step 3)
- Inside the code base (used only during development time of jazz-plugins)

The step name must follow this template:

```
pluginPath:pluginName:taskId
```

- **pluginPath**: This is the source of the task code. As mentioned before, it can reference basically to 3 different sources:

  <br/>

  - **External plugin**: let's say you want to test your plugin, by receiving as raw data the result of step executed by a third-party plugin. That is possible by referencing the plugin path from node-modules. Also, the plugin used here needs to be installed in the project as a dev dependency. For example:


    ```javascript
    const defaultRelativePluginPath = "../..";
    const textLoaderPluginPath = `${defaultRelativePluginPath}/jazz-plugin-textloader/dist`;

    // and in the pipeline step:
    `${textLoaderPluginPath}:jazz-plugin-textloader:load`;
    ```

- **Native plugin**: In Jazz there is a specific plugin that is native, which means is part of the jazz-core lib. This is the Transformer plugin, which is responsible for receiving a javascript object and convert into another object by following a bunch of conversion rules. This is well explained in section ["The Transformer Plugin"](#transformer). It can be used in the pipeline by using the reserved word _native_. For example:

```javascript
native: Transformer: transform;
```

- **Same codebase**: used only in development time. Imagine you are developing a jazz-plugin, you probably will use this option. You can reference a task class in same codebase by using the reserved word _this_. For example, in case we are developing a plugin that sends emails, we will need to test the task _sendmail_, you can reference it by using:

  ```javascript
  this:jazz-plugin-sendmail:sendmail
  ```

**Important**: A single plugin can have as many tasks as it requires to have the work done. It means that each plugin comes with a default internal pipeline. You can configure your jazz-pack to run all tasks inside the plugin by using the following syntax:

```javascript
pluginPath:pluginName:*
```

Notice that instead of informing the taskId in the third part, we can add the \*. This will make Jazz understand that you want to run all the tasks in the plugin default pipeline.

<a name="plugins"></a>

### .plugins [ ]

This is an array of objects. Each object consists in a different plugin. Usually, each plugin that you may have in your pipeline, will need to have an object referenced here. And the reason is that you need to configure the behavior of your plugin, and the tasks inside of the plugin. Let's go the a practical example:

```javascript
 plugins: [
    {
      name: 'jazz-plugin-excelextractor',
      sheet: 'source',
    },
    {
      name: 'Transformer',
      groupIdColumn: 'groupid',
      tasks: {
        transform: {
          rawDataFrom: 'extract',
        },
      },
    },
    {
      name: 'jazz-plugin-textloader',
      tasks: {
        load: {
          rawDataFrom: 'transform',
        },
      },
    },
  ],
```

Let's focus on the first object, that represents a plugin:

```javascript
 {
      name: 'jazz-plugin-excelextractor',
      sheet: 'source',
 }
```

Ok, in here we have 2 properties:

- name: the main property of the plugin. This is a required information for ALL plugins, and the name needs to be equal to the name informed in the pipeline.

- sheet: the second property is specific to the **jazz-plugin-excelextractor**. This basically informs the name of the tab in the excel spreadsheet that needs to be read.

> Note 1: For this case, there is no property called "tasks". That is ok, it is not required. not having it means that I don't need to setup a specific behavior to specific task in the pipeline.

> Note 2: A plugin can have as many properties it requires to have the work done.
> Q. How do I know what are the available properties for each plugin?
> A. You need to look into the jazz-plugin documentation.

> Note 3: If you are a jazz-plugin developer, it is very important to have a well documented plugin!

Let's move on to the second object in the plugin of arrays:

```javascript
{
      name: 'Transformer',
      groupIdColumn: 'groupid',
      tasks: {
        transform: {
          rawDataFrom: 'extract',
        },
      },
    },
```

This relates to the native plugin Transformer.

Notice that there is the name again (which is always required), and another property now called "groupIdColumn". This is a property specific to the transformer plugin.

Basically, it identifies what it the property name in the source object (the one that will be transformed) that is used for grouping records. This will be well explained in the [Transformer Plugin section](#transformer).

Let's put a magnyfing glass in this particular piece:

```javascript
 tasks: {
        transform: {
          rawDataFrom: 'extract',
        },
 }
```

We have the possibility to modify the behavior of a task in the pipeline, by specifying the tasks through the _tasks_ object, followed by the id of the task (in the case above, we are adding a behavior to the task _transform_ that is inside the plugin _Transformer_).

#### Which properties we can setup for the tasks?

These are the properties and methods you can set to ANY tasks (that do not depends on specific plugin behavior), because they relate to the Task Class, that is in the core of the product:

<a name="rawDataFrom"></a>

- **rawDataFrom**: this is the task id of a specific task in the pipeline. Basically it says to the current task from which place it will get the raw data (the data before task processing). In the example above, we are saying to the _transform_ task that it needs to get the raw data from the result of the processing of the task _extract_.

<a name="getRawData"></a>

- **getRawData()**: this is a method that is usually implemented by the plugin. However, you can ovewrite the behavior of this by setting this on the jazz-pack.

##### This is the syntax to ovewrite a method:

```javascript
 tasks: {
        transform: {
          getRawData: () => {
            /*set your logic here to return the raw data
            once this approach is taken, jazz will NOT look into the rawDataFrom property anymore
            example : */
            return ['row 1 - this array will be processed by the task',
                    'row 2 - this array will be processed by the task',
                    'row 3 - this array will be processed by the task',]

          },
        },
 }
```

Let's say you do not want to polute your jazz-pack file with a bunch of inline functions. In this case, you can use the syntax below, which will refer to a function from a specific directory of functions.

```javascript
tasks: {
        transform: {
          getRawData: require('./functions/getDataToTransform')
        },
 }
```

Then, in your project folder, you will have structure below. You just need to make sure your functions directory will be distributed in "dist" folder, and you are good to go:

![tree](./img/tree_functions.png)

<a name="execute"></a>

- **execute()**: this method is tha main one to the task in the plugin. It is required to be implemented by the plugin. Even this one you can completely change it's default behavior by ovewritting this in the jazz-pack level.

> Of course, it provides high level of flexibility, however, one thing you always need to consider is: if you gte yourself changing the plugin behavior too much, maybe you should look for another plugin that would fit better your needs.

<a name="preExecute"></a>

- **preExecute()**: this method is executed before the execute() method. Let's say you want to get the raw data in a step before the execute method, and add something to it. The way to achive this is to ovewritting this method.

Example:

```javascript
 tasks: {
        transform: {
          preExecute: () => {

            /* this is the way to get the data from a specific task  in the pipeline
            here we are getting the data from extract step */
            const defaultRelativePluginPath = '../..';
            const pipelinePath  = defaultRelativePluginPath + '/jazz-core/dist/Pipeline'
            const dataFromExtractStep = require(pipelinePath).getResult("extract");

            /* let's say this is my data at this moment:
            {
              name: 'John',
              lastname: 'Rodriguez'
            }

            Now, let's say that before let the transform task do it's work,
            we want to add more information to it, like this:
            */

           const newDataFromExtractStep =  { ...dataFromExtractStep , email: 'john.rodriguez@jazzmail.com'}

             /*  now what we have in our new data is:
            {
              name: 'John',
              lastname: 'Rodriguez'
              email: 'john.rodriguez@jazzmail.com'
            }

            Finally, all we have to do is to return the new data as a result from preExecute
            */

           return  newDataFromExtractStep;

          }

          /* Important: we want to get the new data in execute
          from the preExecute, not from extract.
          In this case, we ca change the rawDataFrom as below,
          by adding the prefix "pre" and then dash (-) : */
          rawDataFrom: 'pre-extract'


        },
 }
```

<a name="postExecute"></a>

- **postExecute()**: as you can imagine, this method is executed after the execute() method. The way to work with this is exaclty the same as described on the preExecute(). Except that the prefix used to get the result from postExecute is "post".

```javascript
rawDataFrom: "post-extract";
```

- **anything else that the plugin developer allows you to ovewrite**: Yes! Last but not less important, you can change any other behavior that the plugin developer allows you to change. The plugin developer is able to easily configure what are the properties or methods that can be ovewritten in a jazz-pack level.

### .userInputParameters {}

<a name="userInputParameters"></a>

These are parameters used by the Transformer Plugin. Technically speaking, this is a javascript object, that contains other objects. Each of these objects is an input parameter that a final user can inform in the moment they are running a process in Jazz. Typically these basically are behaviors that the user may want to change for different processes they run, by using the same jazz-pack. For example:

```javascript
 userInputParameters: {
    customer_account_currency_code: {
      alias: 'cacc',
      describe: 'Customer Account Currency Code',
      demandOption: false,
      default: 'GTQ',
    },
  },
```

In the example above, the customer_account_currency_code is a field in a source that is being converted by the Transformer Plugin.

In order to understand this, let's go to a real use case:

Imagine that you want to create a jazz-pack that will be responsable for creating a bank file (usually a text file) which will have several fields. In between these fields, you have one that is the country currency, identified as customer_account_currency_code.
The bank file format is exactly the same for different countries. You could create a single jazz-pack for each country. Sure, that is one approach to follow, however, there is a better option.

By adding the user input parameter above, you provide to your jazz-pack the flexibility to create an output file with different content, depending on the parameter.

In this case, if you are using the [jazz command line tool](#jazz-cli), and type:

```bash
jazz --cacc GTQ
```

You can have an output file like this, that will fit the need to Guatemala.

```
GTQ00000000000000000001000000
```

And by typing:

```bash
jazz --cacc BRL
```

You can have an output file like this, that will fit the need to Brazil.

```
BRL00000000000000000001000000
```

> Just another example: Think for example in a plugin that send mails. In the user input parameters we could have the smtp host, mail credentials, port, etc...

These are the properties of each user input:

- alias: this is the optio user will type in cli, as explained before
- describe: it's the meaning of the parameter (will be perfect in a graphical user interface, in the future)
- demandOption (true/false): indicates if parameter is required or not
- default: the default value if user do not inform the value

## Bolierplate Project Structure

In this section, we will explain in details every single file on the boilerplate project.

### package.json

#### dependencies

There is no single dependence for the jazz-pack. Remember that this is nothing more than a file that exports a javascript object. And this file will be referenced by the main jazz application.

Because of this, there are only dev Dependencies on this boilerplate, and they are required simply because during development of the jazz-pack, you will need to test your package. So you need a way to run the pipeline.

#### devDependencies

Contains the dependencies of development of the plugin. These dependencies are used only during development. They are not distributed with the pack.

```json
"devDependencies": {
  "@babel/runtime": "^7.4.5",
    "@babel/cli": "^7.4.4",
    "@babel/core": "^7.4.5",
    "@babel/node": "^7.4.5",
    "@babel/plugin-transform-runtime": "^7.4.4",
    "@babel/preset-env": "^7.4.5",
    "babel-plugin-root-import": "^6.2.0",
    "del": "^4.1.1",
    "eslint": "^5.16.0",
    "eslint-config-airbnb-base": "^13.1.0",
    "eslint-import-resolver-babel-plugin-root-import": "^1.1.1",
    "eslint-plugin-import": "^2.17.3",
    "eslint-plugin-jest": "^22.7.1",
    "eslint-plugin-promise": "^4.1.1",
    "gulp": "^4.0.2",
    "gulp-jsonminify": "^1.1.0",
    "gulp-uglify": "^3.0.2",
    "jazz-core": "^1.0.5",
    "jazz-plugin-excelextractor": "^1.0.3",
    "jazz-plugin-textloader": "^1.0.5",
    "jest": "^24.8.0",
    "prettier-eslint-cli": "^4.7.1"
}
```

This project contains basically 4 groups of dependencies:

1. **Babel core and plugins**: used in the process of build, to transpile es6+ code into "legacy javascript code"
1. **Gulp core and plugins**: task runner used in the process of build and distribution of the package
1. **Eslint**: used to format code by using best practices. In this case, it is using the air-bnb guides.

1. **cryptr**: this is the node module used to encrypt / decrypt the jazz-pack file.

1. **Jazz Dependencies**:

> Important: Always keep in mind that these dependencies **must NOT** be distributed with your final package. They are here only for your development time, so that you can test your package running in a real pipeline.

- **jazz-core**: it is the main engine of jazz. During development it is referenced in node-modules directory. However, once the plugin is distributed, it will reference the jazz-core lib from the client. This means, it will not be distributed with the pack.

- **jazz-plugin-textloader**: in this boilerplate, it is used only for testing the package end-to-end, by receiving data from another step in the pipeline, that is executed by another plugin. Also, used only for testing purposes during development flow. For this reason, it is referenced in the devDependencies, not in main dependencies. This means, it will not be distributed with the jazz-pack.

- **jazz-plugin-excelextractor**: in this boilerplate, it is used only for testing the package end-to-end, by reading an excel file and sending resulted data to another step in the pipeline, that is executed by another plugin. Also, used only for testing purposes during development flow. For this reason, it is referenced in the devDependencies, not in main dependencies. This means, it will not be distributed with the jazz-pack.

#### npm scripts

```json
"scripts": {
    "build": "babel src -d build && gulp minifyJson",
    "prebuild": "gulp cleanBuild",
    "lint": "eslint 'src/**/*.js' --fix",
    "git": "git add . && git commit -m %RANDOM% && git push",
    "test": "jest --watch",
    "dist": "npm run build && gulp dist",
    "encrypt": "gulp encrypt",
    "start": "node ./build/__tests__/run-cli.js --configFile ./build/pack-config --sourceFile ./build/__tests__/source.xlsx --outputFile ./build/__tests__/target.txt",
    "dev": "nodemon --exec babel-node ./src/__tests__/run-cli.js --configFile ./src/pack-config  --sourceFile ./src/__tests__/source.xlsx --outputFile ./src/__tests__/target.txt --debug",
    "dev-enc": "nodemon --exec babel-node ./src/__tests__/run-cli.js --configFile ./src/pack-config.sec  --sourceFile ./src/__tests__/source.xlsx --outputFile ./src/__tests__/target.txt --debug --decryptKey JazzIsAwesome"
  },
```

- build: used to transpile the code into legacy javascript code using babel 7. Saves the code into build folder
- prebuild: called automatically by npm before each build. in this case it excludes all the files inside the build directory
- lint: used to run the eslint checks
- git: used to commit and push changes into a git repository
- start: runs the run-cli.js file in "production mode"
- test: can be used for unit testing, if you use decide to use a tool like jest (which we highly recommend)
- dev: runs the run-cli.js file by using nodemon. This improves the development flow, since on each change, it will re-run automatically. This command calls the command line `jazz` with some input parameters:
  - configFile: references to the jazzpack file (in this case ./src/pack-config)
  - outputFile: input parameter specific for the plugin jazz-plugin-textloader. Represents the file path that text file will be saved after the task is precessed
- dist: calls the build and then the task gulp _dist_. The task dist will perform the following tasks in the sequence: (see gulpfile.js file):
  - cleanDist: excludes all files from dist folder
  - minifyJson: minifies the json files that may exist in the .src folder
  - uglifyJs: uglifies the js files that are in the build folder and saves into dist folder
  - replaceDist: some files may have a different version on development time than on productin. This task replaces some files that need to be different on distribution folder.
- encrypt: if you decide to encrypt your pack, you simple need to run this command after the _dist_ command

### The \_\_tests\_\_ directory

All files inside this folder will be excluded from distribution package. They are used only during development time, for testing purposes.

### run-cli.js

This file contains only 2 lines of code.
It calls the run method inside the CliLoader class.
The CliLoader class is available in the jazz-core lib, and is responsible for creating a _jazz_ command line tool.

The run method is responsible for starting the pipeline. It means it will read the jazz-pack file, mount the pipeline with all the tasks and call each task in the sequence.

```javascript
import CliLoader from "jazz-core/dist/CliLoader";
CliLoader.run();
```

By running the `bash npm run dev`, it will make possible testing the jazz-pack inside the pipeline.

> Note: for running the pipeline, you need to have the jazz-core and the jazz-plugins used installed in your project as a devDependency. That is the case of this boilerplate project:

```json
 "devDependencies": {
  "jazz-core": "^1.0.9",
  "jazz-plugin-excelextractor": "^1.0.3",
  "jazz-plugin-textloader": "^1.0.5",
}
```

## How to distribute your jazz-pack

First thing to do is to pack your jazz-pack. :-(
This boilerplate already has a npm script that will do the work for you. All you need to do is to open the terminal and on the project folder, type:

```bash
npm run dist
```

On package.json file you can see that this command will run the commands below, in sequence:

```bash
npm run build && gulp dist
```

- npm run build: the build command will run the babel transpiler (using version 7) that will convert your code from new javascript (+ ES6) to "old" javascript (< ES6).

- gulp dist: when opening the gulfile.js, the dist task does the following:

  - cleanDist: excludes all files from dist folder
  - minifyJson: minifies the json files that may exist in the .src folder
  - uglifyJs: uglifies the js files that are in the build folder and saves into dist folder
  - replaceDist: some files may have a different version on development time than on production. This task replaces some files that need to be different on distribution folder.

Ok, so now you tested your package, created the distribution package and you are good to go for production. In soon future, the idea is to have an specific marketplace for jazz-packs and jazz-plugins in which the users will be able to download free or buy the plugins and packs (almost like the VSCode market place of extensions). At this moment, this feature is not developed, so that, the only way to publish the jazz packs is through well known npm.

So, as a pre-requisite, you need to have an account created on [https://www.npmjs.com](https://www.npmjs.com/).

Then, all you need to do is to run the command in the terminal:

```bash
npm publish
```

The .npmignore file will guarantee that only the dist folder will be published. Of course, if your intention is to charge for the pack, by publishing it on npm is still not the best solution. As mentioned, the Jazz MarketPlace is a work in progress.

Perfect, your jazz-pack is developed and available for others to use.

### How do I encrypt the jazz-pack?

If you want to encrypt your jazz-pack (which will hide your code form end users), you can type the command:

```bash
npm run encrypt
```

This is the same to run the _gulp encrypt_ command, which will run the gulp task named _encrypt_ (check the encrypt function inside the gulpfile.js)

When you look at the gulpfile.js, you can see that this task gets the pack-config.js file (the original) and creates a pack-config.sec file (the encrypted). And by the end, deletes the original file from the dist folder (of course, you won't want the original file there, only the encrypted one).

```javascript
const configFile = "./src/pack-config.js";
const encryptedFile = "./src/pack-config.sec";
```

Of source, if you decide to change the name of the pack-config.js file, that is ok, but keep in mind that you will need to change the npm scripts and the gulpfile.js, that have references to the pack-config.js file.

### Do I need to encrypt the pack?

Well, as explained, the boilerplate project will only provide you an easy way to perform the encryption. However, it's up to you if you will protect your package code or will keep this open source.

There are 2 options:

- You can release the package and provide users the source code, which will allow anyone to change your original configuration and use for their own purpose. In this case, encryption will not be necessary;

- depending on complexity of your package, you could develop it and charge for the downloads in future jazz platform (to be developed). In this case, that will be a good idea to encrypt your jazz-pack.

> Note: at this moment, the feature of encryption that will keep your code safe is not that "safe" yet. This capability still needs to be improved. In the future, once we have a market place to distribute the packages, the users will need to be logged in to download and install the packages. In this moment, we will be able to improve the feature.

<a name="transformer"></a>

## The Transformer Plugin

This is a plugin that comes with jazz-core. It is embedded inside same lib, so once you download the jazz-core, the Transformer plugin will come together, in same package.

The main idea of any plugin in Jazz is basically the same: get some data, do something with it and deliver a result to the user or to the next step in the pipeline. This is not different with the Transformer plugin.

Basically, it receives a raw data, and based on a list of instructions (configured in the jazz-pack file), it converts this data into a different set of data.

<br/>

![diagram](img/diagram1.png)

### How to configure the jazz-pack to use the transformer plugin?

Let's use a real world example to explain. You have an excel spreadsheet with some data, and you need to transform this into a text file, by following some specifications on position, size, data types, mappings, ...

Your excel spreadsheet, looks like the one below (by the way, this is the example file source.xlsx you can find in the **tests** folder ):

![excel](img/excel.png)

This is a three step pipeline:

1. extract data form excel
2. transform data
3. load data into text file

We will not go through details into steps 1 and 3. The only thing you need to understand is that these steps are accomplished by 2 different plugins (the _jazz-plugin-excelextractor_ and the _jazz-plugin-textloader_). By the way, this is the use case implemented in this boilerplate.

OK, so moving to the step 2, which is the important one here, for the purpose of this tutorial.

1. The first thing you need to do is to use the plguin Transformer, more specifically the _transformer_ task as a step in your [pipeline](#pipeline).

```javascript
pipeline: [
  `${excelExtractorPluginPath}:jazz-plugin-excelextractor:extract`,
  `${excelExtractorPluginPath}:jazz-plugin-excelextractor:validate`,

  /* here is the transformer plugin, task transform */
  `${transformerPluginPath}:Transformer:transform`,

  `${textLoaderPluginPath}:jazz-plugin-textloader:load`
];
```

2. Second thing is to inform the source from raw data to Transformer plugin, through the [plugins](#plugins) property, like this:

```javascript
 plugins: [
   /* other plugins...*/
    {
      name: 'jazz-plugin-excelextractor',
      sheet: 'source',
    },

    {
      name: 'Transformer',
       /* here you are telling the Transformer plugin that it needs to use the property in sourced object named groupid to identify a group of records - this is not required information  */
      groupIdColumn: 'groupid',
      tasks: {
        transform: {
          /* here you are telling the Transformer plugin that it's source is the result of the task extract  */
          rawDataFrom: 'extract',
        },
      },
    },
    /* other plugins...*/
    {
      name: 'jazz-plugin-textloader',
      tasks: {
        load: {
          rawDataFrom: 'transform',
        },
      },
    },
  ],
```

3. Now that your transformer plugin is in your pipeline and knows from here to get the data, it's time to tell the plugin what to do with this data.

We can achieve this by adding the _groups_ array in the root of our jazz-pack.

#### The _groups_ array

```javascript
groups: [
  ...
]
```

Ok, so, now let's start adding the groups into the array. Each group is a javascript object, like this:

```javascript
groups: [
   {
      id: 'main',
      parent: ''
      fieldDelimiter: '|'
      sizeMode: 'fixed',
      data: {
        ...
      },
   }
]
```

Notice here that there are 3 properties:

- id: the name of the group. It is a required information. Also, it needs to be an unique value in the pack.
- sizeMode: can be _fixed_ or _variable_. Default value is _fixed_
  This determines how the information will be positioned in the group.
- fieldDelimiter: usually, when the sizeMode is _variable_, a delimiter char is required between values. **optional**.
- parent: the name of the parent group of this group. **optional**. _It's used only in more complex use cases, in which the target data is composed by multiple groups of records_
- data: javascript object containing the data configuration for the group

##### The data object

```javascript

 groups: [
    {
      id: 'main',
      sizeMode: 'fixed',
      data: {
        hasHeader: false,
        hasDetail: true,
        detailGroup: 'pay',
        hasTrailer: true,
        header: {
          columns: [
            ...
          ],
        },
        detail: {
          columns: [
            ...
          ],
        }
        trailer: {
          columns: [
            ...
          ],
        },
      },
    },

```

It contains the following properties:

- hasHeader (true/false): Indicates if the group has a header record or not
- hasDetail (true/false): Indicates if the group has details records or not (usually stays between the header and trailer)
- hasTrailer (true/false): Indicates if the group has a trailer record or not
- detailGroup: The identifier of the group in the source data
- header { columns [ ... ] }: this object contains an array named columns. Consists in the columns that compose the header record
- trailer { columns [ ... ] }: this object contains an array named columns. Consists in the columns that compose the trailer record
- detail { columns [ ... ] }: this object contains an array named columns. Consists in the columns that compose the details record

##### The columns array

This array will contain all the columns that the target data will have. Each column is represented by a javascript object:

```javascript
 columns: [
            {
              id: 'record_type',
              description: 'Record Type',
              required: 'on',
              type: 'text',
              size: 3,
              defaultValue: 'TRL',
            },
            {
              ... /*2nd column */
            }
            {
              ... /*3rd column */
            }
          ...
 ]
```

Each column object contains a list of properties that will define how the output value will be delivered.

- **id**: the name of the column. This will be the property name in the object which will containt the transformed data.
- **description**: describes the meaning of the information.
  _It's not used by the logic in an aspect. Will be usefull when we have a user interface implemented_.
- **required (on/off)**: if this flag is set to _on_, the transformation process will raise an errorif source data is empty or do not exist
- **defaultValue**: when informed, the transformer will not look into source data. Will just consider this default value in the output.
- **type**: possible values are:
  - text
  - number
  - date
- **size**: represents the number of characters the column will have in the transformed data. This works only when _sizeMode_ is set to _fixed_.
- **fillMode**: possible values:

  - zeros_on_left: will fill the value with zeros on the left side until complete the number of characters defined on \_size\_ property
  - zeros_on_right: will fill the value with zeros on the right side until complete the number of characters defined on \_size\_ property
  - spaces_at_end: will fill the value with spaces on the end, until complete the number of characters defined on \_size\_ property.

If the fillMode is not informed, the transformer will consider:

- if _type_=_text_ or _date_, fillMode = spaces_at_end
- if _type_=_number_, fillMode = zeros_on_left

- **fillCondition**: this is a function that by default, receives the row being processed in raw dat. It's required that this function return a boolean (true/false). If true, information will be feed. If false, will keep it as empty. Example:

  ```javascript
  fillCondition: ({ transaction_code }) => transaction_code == '71',
  ```

  In the example, the information will be empty if in the same record being processed, the value in property transaction_code is different from 71.

- **noDecimalDigit (true/false)**: if type is number and this value is set to _true_, will exclude the decimals from the number. Default is _value_. Example: 123,45 or 123.45 will become 12345.

- **empty (true / false)**: if true, output will fill with blanks. Default is _false_. If _defaultValue_ is set, this property will not work.
- **dateFormat (wip)**: defines the date format for columns typed as _date_.

> _The dateFormat is still a work in progress, so in order to achieve the right date format, this needs to be feed in the final format in the source data. Another option could be to ovewrite the [preExecute](#preExecute) method and transform the date format before move to the transform step_

- **excludeChars**: An array of characters that must be excluded from the output. The example below will remove the chars from the output.

  ```
  excludeChars: ['-', '.', '/'],
  ```

- **removeAccents (true/false)**: If set to _true_, removes the accents from the text on the output. Default value is _false_.
- **removeSpecialChars (true/false)**: If set to _true_, removes the special chacarters from the text on the output. Default value is _false_.
- **caseType (lower / upper)**: if lower, will transform output into lowercase. If set to upper, will set the output to uppercase. If not set will consider the data from the source as is.

- **transformType**: This property is a very specific one. If informed, it will be required to inform the **transform** object. Depending on the option informed, the **transform** object will have different properties.

The possible values are:

- **source**: represents that the output will be originated from a source value in the raw data object.
  - transform:
    - source: the name of the property in the raw data.

In the example below, we are telling jazz that the _transaction_code_ field in raw data must be used to deliver the transformed data in the _transaction_code_ field in the output object. They have the same name in here, but this is not a rule.

```javascript
     transformType: 'source',
     transform: {
       source: 'transaction_code',
     }
```

- **mapping**: represents that the output value will be the result of a conversion table (the conversion table will be a json file that needs to be distributed with the jazz-pack).

  - transform:
    - sourceOnMappingTable: the name of the column in the conversion table that holds the result value.
    - caseSensitive (true/false): if set to _true_, the comparign between sourced data and conversion table will be case sensitive.
    - mappingData: indicates the json file that will contain the conversion table.
    - tableName: the name of the table in the json file. This allows the same json file to handle more than one conversion table.
    - mappingKeys: an array of keys to be used to find the result information. Why this is an array? Let's say that in the sourced data you have 2 fields that will compose a single result on the conversion table. Yo could achieve this result by composing the keys like this:
      ```
      [
       { key: 'name', source: 'bankname' },
       { key: 'name2', source: 'bankname2' }
      ]
      ```
    - useOriginalOnNotFound (true/false): if corresponding value not found and this is set to true, the source value will be considered in the output. If false (default), and the corresponding is value, an error will be raised.
    - errorOnMultiple (true/false): If set to true, an error will be raised when 2 or more corresponding values are found in the conversion table. If false, output value will consider the first record found in conversion table.

In the example below, we are telling jazz to use the banks.json file as conversion table. Also we are saying that the _code_ field in the table is the value to be used as result.

```javascript
 transformType: 'mapping',
     transform: {
        sourceOnMappingTable: 'code',
        caseSensitive: false,
        mappingData: require('./mappings/banks.json'),
        tableName: 'banks',
        mappingKeys: [{ key: 'name', source: 'bankname' }],
        useOriginalOnNotFound: true,
        errorOnMultiple: true,
   }
```

- **input**: represents that the value will be taken from an [userInputParameter](#userInputParameters).

  - transform: not required

- **parent**: represents that the output value will be the sourced to a field that is part of the parent structure. For example. Think on the follwoing structure of an output file:

```
parent: value1|value2|value3
child:  value4|value5|(map to field value1 that is in the parent record)
```

- transform:
  - groupId: the name of the parent group
  - sectionId: the section in the parent group (field can be mapped from the header, detail or trailer)
  - source: the field name in source data

```javascript
   transform: {
      groupId: 'pay',
      sectionId: 'detail',
      source: 'transaction_sequence_number',
   },
```

- **calculation**: Works better in trailer section. Useful when you want to count or summarize the records, for example, in a trailer record of your output file.

  - transform:

    - calculationType: possible values are...
      - _count_: counts the number of records
      - _sum_: summarizes the value from a specific field (field name will be indicated in property _source_)
    - source: indicates the field name that will be used to summarize, when calculationType equal to _sum_.
    - calcCondition: a function that receives the raw data and returns a boolean (true / false). Useful when you want your final calculation result to consider some conditions.
      In the example below, it will consider in the calculation only the records in which the groupid field is equal to _pay_:

    ```javascript
     transform: {
                calcCondition: ({ groupid }) => groupid === 'pay',
                calculationType: 'count',
     }
    ```

    The calcCondition value can be a pure pinline function, or a string referencing to a js file that will contain the function.

    Then, you just need to make sure the file that contains the function will be distributed with your jazz-pack.

    ![tree_functions](img/tree_functions.png)

* **sequence**: useful when you have a field that will be a sequenced number, like below. The 3rd field is a sequenced number.

value1|value2|<span style="color: blue">00001</span>
value1|value2|<span style="color: blue">00002</span>
value1|value2|<span style="color: blue">00003</span>
value1|value2|<span style="color: blue">00004</span>

- transform:
  - groupId: identifies the group to be considered in the sequence (see example below for more clarity)

The _transform_ option is required only when the sequence needs to consider a group of records. Note that the sequence restarts on each new group. This can be achieve by using the _sequence_ option combined with the options described above. In the example below, the _groupId_ value would be equal to _group1_ and _group2_, depending on the sequence.

group1|value1|value2|<span style="color: blue">00001</span>
group1|value1|value2|<span style="color: blue">00002</span>
group1|value1|value2|<span style="color: blue">00003</span>
group2|value1|value2|<span style="color: red">00001</span>
group2|value1|value2|<span style="color: red">00002</span>
group2|value1|value2|<span style="color: red">00003</span>

- **complex**: let's say that none of the out-of-box options described fit to your transformation use case. In this case you can use the complex option. This will provide the possibility to reference to a function that will transform the data for you. This function will receive the raw data as an argument.

  - transform:

    - func: you can set an inline function in the jazz-pack file directly. Example:

    ```javascript
     transformType: 'complex',
              transform: {
              func: (data) => {
                /* do something with the original data */
                /* destructure to get the required field  */
                const { theField } = data;

                return newData;
               }
              },
    ```


    - functionRef: a string with the function path. It's used when you don't want to polute the jazz-pack file with inline funcitons. You can this option to indicate a separatedjs file that will contain the function.

      ![tree_functions](img/tree_functions.png)

    Then, you just need to make sure the file that contains the function will be distributed with your jazz-pack.
